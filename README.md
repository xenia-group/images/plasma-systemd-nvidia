# Plasma systemd

Plasma systemd Xenia image with added Nvidia drivers.

## Overview

**OS:** Gentoo Linux

**DE:** KDE Plasma

**Init system:** systemd

## Details

`./config` - Portage config (`/etc/portage`)

- `config/local` - Added by the specific image
- `config/config` - [Global config on all Nvidia images](https://gitlab.com/xenia-group/images/common/config-nvidia)

`./overlay` - Filesystem overlay

- `overlay/local` - Overlay for the specific image
- `overlay/overlay` - [Global overlay on all Nvidia images](https://gitlab.com/xenia-group/images/common/overlay-nvidia)

`./global` - [Global Nvidia config](https://gitlab.com/xenia-group/images/common/global-nvidia)

- `fsscript.sh` - Script that runs at end of image creation
- `make.conf.append` - Options added to make.conf
- `profile` - Gentoo profile (default, overwritten by local)
- `package.list` - Default package list

`./` - Local settings

- `fsscript.sh` - Script that runs at end of image creation, after global fsscript
- `package.list` - Additional packages for the specific image

